﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Car.Models
{
    public class Comment
    {
        [Key]
        public int Id {set; get;}
        public String Title{set; get;}
        public String Body{set;get;}
    }
}