﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
namespace Car.Models
{
    public class Post
    {
        [Key]
        public int Id { set; get; } // gán và lấy giá trị
        public String Title { set; get; }
        public String Body { set; get; }
    }
    public class BlogDBContext : DbContext
    {
        public DbSet<Post> Posts {set; get;}
         public DbSet<Comment> Comments { set; get; }
    }

}