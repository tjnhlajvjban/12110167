﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Post
    {
        [Required]
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "Title phải có từ 20 đến 500 ký tự", MinimumLength = 20)]
        public String Title { set; get; }
        [Required]
        [StringLength(5000, ErrorMessage = "Body phải có tối thiểu 50 ký tự", MinimumLength = 50)]
        public String Body { set; get; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        
        public virtual ICollection<Post> Posts { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
    }
}